import Globals
import Debug

def prase_usr_pass(data):
	username_len = data[Globals.LOC_OF_USER_LEN ]
	username_starts = Globals.LEN_OF_LEN + Globals.LOC_OF_USER_LEN
	username = data[username_starts: username_starts + username_len]
	password = data[username_starts + username_len:]
	return username, password

def parse_cookie(data):
	 return data[:Globals.LEN_OF_COOKIE]

def parse_conformation_cookie(data):
	return data[Globals.LOC_OF_CONFORMATION], data[Globals.LOC_OF_CONFORMATION + Globals.LEN_OF_CONFORMATION:]
