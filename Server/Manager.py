import socket
from multiprocessing import Process, Manager
from ClientHandler import ClientHandler
import sqlite3
import Debug
import Globals

class Manager2:
	host = '127.0.0.1'
	port = 28050

	def __init__(self):
		self.manager = Manager()
		self.db_mtx = self.manager.Lock()
		self.pending_login_mtx = self.manager.Lock()
		self.users_data_mtx = self.manager.Lock()
		self.db = sqlite3.connect("db")
		self.cursor = self.db.cursor()
		self.db_mtx.acquire()
		try:
			self.cursor.execute("CREATE TABLE IF NOT EXISTS users(username STRING NOT NULL PRIMARY KEY, password BLOB NOT NULL, cookie BLOB);")
			self.db.commit()
		finally:
			self.db_mtx.release()

	def __del__(self):
		try:
			self.db_mtx.acquire()
			self.db.commit()
			self.db.close()
		except Exception:
			pass
		finally:
			self.db_mtx.release()

	def manage(self):
		clients = []
		Debug.mpl.info("main")
		processes = []
		mini_dicts = self.manager.dict()
		users_data = self.manager.dict()
		with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
			s.bind((self.host,self.port))
			while True:
				s.listen()
				conn, addr = s.accept()
				clients.append(ClientHandler(conn))
				mini_dicts[conn.getpeername()] = self.manager.dict()
				processes.append(Process(target=clients[-1].manage_connection, args=(users_data, self.db_mtx, self.pending_login_mtx, self.users_data_mtx, mini_dicts)))
				processes[-1].start()

if __name__ == "__main__":
	manage = Manager2()
	manage.manage()

