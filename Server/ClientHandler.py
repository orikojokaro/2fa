import Globals
import sqlite3
import Parser
from argon2 import PasswordHasher
import secrets
from struct import pack
import Debug
import multiprocessing



class ClientHandler:

	def __init__(self, conn):
		self.conn = conn
		self.hasher = PasswordHasher()
		self.is_logged_in = False
		self.username = None
		self.users_data = None

	def __del__(self):
		self.db.commit()
		self.db.close()
		self.conn.close()

	def insert_user_data(self, username, logged_in_machines = 0, is_2fa_needed = False, cv_2fa = None, is_confirmed = False):
		print(self.mini_dict[self.conn.getpeername()])
		my_mini_dict = self.mini_dict[self.conn.getpeername()]
		my_mini_dict["logged_in_machines"] = logged_in_machines
		my_mini_dict["is_2fa_needed"] = is_2fa_needed
		my_mini_dict["cv_2fa"] = cv_2fa
		my_mini_dict["is_confirmed"] = is_confirmed
		self.mini_dict[self.conn.getpeername()] = my_mini_dict#TODO is this necessary?
		self.users_data[username] = self.mini_dict[self.conn.getpeername()]
	#self.users_data[username] = self.manager.dict({"logged_in_machines":logged_in_machines, "is_2fa_needed":is_2fa_needed, "cv_2fa":cv_2fa, "is_confirmed":is_confirmed})

	def send_with_header(self, data):
		length = pack('H', len(data) + Globals.HEADER_SIZE)#TODO ensure this will be at the correct order(little endian)
		to_send = bytearray(length) + bytearray(data)
		self.conn.send(to_send)

	def extract_data(self, msg):
		return msg[2: Globals.BYTE_OPTIONS * msg[1] + msg[0]]#little endian processor!

	def are_details_correct(self, username, password):
		self.db_mtx.acquire()
		try:
			self.cursor.execute("SELECT password FROM users WHERE username = ?;", (username,))
		finally:
			self.db_mtx.release()
		row = self.cursor.fetchone()
		if row == None: return False#TODO ensure this needs to equal none and not len(row) == 0
		elif self.hasher.verify(row[0], password): return True#only asked for the password
		else: return False

	def get_cookie_owner(self, cookie):
		self.db_mtx.acquire()
		try:
			Debug.mpl.info("a")
			self.cursor.execute("SELECT username FROM users WHERE cookie = ?;", [cookie])
			Debug.mpl.info("b")
		finally:
			self.db_mtx.release()
		row = self.cursor.fetchone()
		if row == None: return None
		else: return row[0]

	def create_cookie(self):
		cookie = []
		for i in range(0, Globals.LEN_OF_COOKIE):
			cookie.append(secrets.SystemRandom().randint(0, Globals.BYTE_OPTIONS - 1))
		return bytearray(cookie)

	def handle_login_response(self, data):
		code, cookie = Parser.parse_conformation_cookie(data)
		Debug.mpl.info(("14.9", self.cursor))
		if self.get_cookie_owner(cookie) != None:
			Debug.mpl.info("Cookie is correct")
			self.users_data_mtx.acquire()
			Debug.mpl.info("15")
			my_data = self.users_data[self.username]
			my_data["is_2fa_needed"] = False
			Debug.mpl.info("16")
			my_data["is_confirmed"] = bool(code)
			self.users_data[self.username] = my_data
			self.users_data[self.username]["cv_2fa"].notify_all()
			Debug.mpl.info("17")
			self.users_data_mtx.release()
			Debug.mpl.info("sending response to authenticator")
			self.send_with_header([Globals.TRUE])
		else:
			Debug.mpl.info("Cookie is not correct")
			self.send_with_header(bytearray([Globals.FALSE, Globals.AUTH_FAILURE]) + str.encode("Incorrect cookie. Failed to authenticate your identitiy."))

	#returns none if there is no cookie
	def get_cookie(self, username):
		Debug.mpl.info("getting cookie")
		self.db_mtx.acquire()
		try:
			self.cursor.execute("SELECT cookie FROM users WHERE username =?;", (username,))
		finally:
			self.db_mtx.release()
		return self.cursor.fetchone()[0]

	def handle_sync(self):
		Debug.mpl.info("entered sync")
		self.users_data_mtx.acquire()
		Debug.mpl.info("17, %s"%(str(self.users_data[self.username])))
		is_needed = self.users_data[self.username]["is_2fa_needed"]
		Debug.mpl.info("18")
		self.users_data_mtx.release()
		Debug.mpl.info("sync")
		if is_needed:
			Debug.mpl.info("sending that sync is needed")
			self.send_with_header([Globals.SUCCESS, Globals.TRUE])
		else:
			self.send_with_header([Globals.SUCCESS, Globals.FALSE])

	def handle_login_details_auth(self, data):
		username, password = Parser.prase_usr_pass(data)
		if not self.are_details_correct(username, password):
			self.send_with_header(bytes([Globals.FAIL]) + bytes([Globals.AUTH_FAILURE]) + str.encode("Incorrect login details"))
			return Globals.FAIL
		else:
			Debug.mpl.info("4.5")
			self.users_data_mtx.acquire()
			Debug.mpl.info("in login function, checking if the username entry exists.username: %s keys: %s" % (str(username), str(self.users_data)))
			if username in self.users_data.keys():
				Debug.mpl.info("4.75")
				Debug.mpl.info("in login function, the username entry exists. are_machines_connected: %s" % (self.users_data[username]["logged_in_machines"]))
				are_machines_connected = bool(self.users_data[username]["logged_in_machines"])
			else:
				are_machines_connected = False
				self.insert_user_data( username=username)
				Debug.mpl.info("5")
			self.users_data_mtx.release()
			if are_machines_connected:
				Debug.mpl.info("6")
				self.pending_login_mtx.acquire()#to ensure that no 2 pending requests will be handeled simultanously, what can create a situation where 2 machines with the same user will attempt a login and not be blocked.
				self.users_data_mtx.acquire()
				is_needed = self.users_data[username]["is_2fa_needed"]
				self.users_data_mtx.release()
				if is_needed:
					self.send_with_header(bytearray([Globals.FAIL]) + bytearray([Globals.EXCEEDING_PENDING_REQUESTS_LIMIT]) +
										  str.encode("There can only be one pending 2FA request at a time. Confirm your login from a different machine first."))
					return Globals.FAIL
				self.send_with_header([Globals.FURTHER_AUTH])
				self.username = username
				return Globals.FURTHER_AUTH
			else:
				cookie = self.get_cookie(username)
				if cookie == None:
					cookie = self.create_cookie()
					self.db_mtx.acquire()
					try:
						self.cursor.execute("UPDATE users SET cookie = ? WHERE username = ?;",
											(bytearray(cookie), username))
						self.db.commit()
					finally:
						self.db_mtx.release()
				self.send_with_header(bytes([Globals.SUCCESS]) + cookie)
				self.is_logged_in = True
				self.username = username
				self.users_data_mtx.acquire()
				user_entry = self.users_data[self.username]
				user_entry["logged_in_machines"] += 1
				self.users_data[self.username] = user_entry
				self.users_data_mtx.release()
				return Globals.SUCCESS

	def handle_sign_up(self, data):
		username, password = Parser.prase_usr_pass(data)
		if len(username) < Globals.MIN_USERNAME_LEN:
			self.send_with_header(bytearray([Globals.FAIL]) + bytearray([Globals.USERNAME_TOO_SHORT]) +  str.encode("Username too short"))
			return Globals.FAIL
		elif len(password) < Globals.MIN_PASS_LEN:
			self.send_with_header(bytearray([Globals.FAIL]) + bytearray([Globals.PASS_TOO_SHORT]) + str.encode("Password too short"))
			return Globals.FAIL

		failed = False
		cookie = self.create_cookie()
		try:
			self.db_mtx.acquire()
			try:
				self.cursor.execute("INSERT INTO users (username, password, cookie) VALUES(?,?, ?);",(username, self.hasher.hash(password), cookie))
			except sqlite3.IntegrityError:
				failed = True
			self.db.commit()
		finally:
			self.db_mtx.release()
		if failed:
			self.send_with_header(bytes([Globals.FAIL]) + bytes([Globals.USERNAME_EXISTS])+ str.encode("Username details exist"))#TODO are the boxy brackets neccessary?
			return Globals.FAIL
		self.send_with_header(bytes([Globals.SUCCESS]) + cookie)
		self.is_logged_in = True
		self.username = username
		self.users_data_mtx.acquire()
		self.insert_user_data( username=username, logged_in_machines=1)
		self.users_data_mtx.release()
		return Globals.SUCCESS

	#returns int, Globals.FUrther_Auth if waiting for further authenctication via 2fa, Globals.SUCCESS if successful, and Globals.FAIL if failed.
	def start_sign_up_or_in(self):
		while True:
			Debug.mpl.info("3")
			try:
				msg = self.conn.recv(Globals.MAX_SIZE)
				data = self.extract_data(msg)
				if data[0] == Globals.SIGN_UP:
					res = self.handle_sign_up(data)
					if res == Globals.SUCCESS:
						break
					else:
						continue
				elif data[0] == Globals.LOGIN_DETAILS_AUTH:
					Debug.mpl.info("4")
					res =  self.handle_login_details_auth(data)
					Debug.mpl.info("6")
					if res == Globals.SUCCESS:
						break
					elif res == Globals.FURTHER_AUTH:
						return Globals.FURTHER_AUTH
					else:
						continue
				elif data[0] == Globals.COOKIE_AUTH:
					Debug.mpl.info("7")
					cookie = data[Globals.SUCCESS_CODE_LEN:]
					cookie_owner = self.get_cookie_owner(cookie)
					if cookie_owner is not None:
						self.send_with_header(bytes([Globals.SUCCESS]))
						self.is_logged_in = True
						Debug.mpl.info("7.5")
						self.username = cookie_owner
						self.users_data_mtx.acquire()
						Debug.mpl.info("7.75")
						if self.username not in self.users_data:
							self.insert_user_data( username=self.username)
						Debug.mpl.info("abou to finish cookie aoth. users_data: %s" % str(self.users_data))
						user_entry = self.users_data[self.username]
						Debug.mpl.info(user_entry.keys())
						user_entry["logged_in_machines"] +=1
						Debug.mpl.info("7.8")
						self.users_data[self.username] = user_entry
						Debug.mpl.info("Finished cookie aoth. users_data: %s"%(str(self.users_data)))
						self.users_data_mtx.release()
						Debug.mpl.info("8")
						return Globals.SUCCESS
					else:
						self.send_with_header(bytearray([Globals.FAIL]) + bytes([Globals.AUTH_FAILURE]) + str.encode("Incorrect cookie"))
				else:
					self.send_with_header(bytearray([Globals.FAIL]) + bytes([Globals.MISC_ERROR]) + str.encode("Incorrect Codes"))
			except Exception as exc:
				try:
					self.send_with_header(bytes([Globals.FAIL]) + bytes([Globals.MISC_ERROR]) + str.encode("Error: ") + str.encode(str(exc)))
				except ConnectionError:
					pass
				if self.is_logged_in:
					self.users_data_mtx.acquire()
					if self.username in self.users_data:
						self.users_data[self.username]["logged_in_machines"] -= 1
					self.users_data_mtx.release()
					self.is_logged_in = False
				return Globals.FAIL
		return Globals.SUCCESS

	def manage_connection(self, data, db_mtx, pending_login_mtx,users_data_mtx, mini_dict):
		Debug.mpl.info("1")
		self.db_mtx = db_mtx
		self.pending_login_mtx = pending_login_mtx
		self.users_data_mtx = users_data_mtx
		self.manager = multiprocessing.Manager()
		self.mini_dict = mini_dict
		Debug.mpl.info(self.mini_dict.keys())
		self.users_data_mtx.acquire()
		self.users_data = data
		Debug.mpl.info(self.users_data.keys())
		self.users_data_mtx.release()
		#multiprocessing must not need to pickle sqlite obj and locks.
		self.db = sqlite3.connect("db")
		self.cursor = self.db.cursor()
		Debug.mpl.info("2")
		if self.start_sign_up_or_in() == Globals.FURTHER_AUTH:
			Debug.mpl.info("9")
			self.users_data_mtx.acquire()
			my_data = self.users_data[self.username]
			my_data["cv_2fa"] = self.manager.Condition(self.users_data_mtx)
			my_data["is_2fa_needed"] = True
			self.users_data[self.username] = my_data
			self.pending_login_mtx.release()
			Debug.mpl.info("gonna wait")
			self.users_data[self.username]["cv_2fa"].wait()
			Debug.mpl.info("stopped waiting, is_confirmed is {0}".format(self.users_data[self.username]["is_confirmed"]))
			if not self.users_data[self.username]["is_confirmed"]:
				Debug.mpl.info("12")
				my_data = self.users_data[self.username]
				my_data["is_2fa_needed"] = False
				self.users_data[self.username] = my_data
				self.users_data_mtx.release()
				self.send_with_header(bytearray([Globals.FAIL, Globals.LOGIN_DENIED]) + str.encode("The permission to log in has not been given.") )
				return None
			else:
				Debug.mpl.info("13")
				user_entry = self.users_data[self.username]
				user_entry["logged_in_machines"] += 1
				self.users_data[self.username] = user_entry
				Debug.mpl.info("14")
				self.users_data_mtx.release()
				try:
					self.send_with_header(bytearray([Globals.SUCCESS]) + self.get_cookie(self.username))
				except Exception as exc:
					try:
						self.send_with_header(
							bytes([Globals.FAIL]) + bytes([Globals.MISC_ERROR]) + str.encode("Error: ") + str.encode(
								str(exc)))
					except ConnectionError:
						pass
					self.users_data_mtx.acquire()
					self.users_data[self.username]["logged_in_machines"] -= 1
					self.users_data_mtx.release()
					return None
		while True:
			try:
				try:
					msg = self.conn.recv(Globals.MAX_SIZE)
				except ConnectionResetError or ConnectionAbortedError:
					break
				data = self.extract_data(msg)
				if data[0] == Globals.SYNC:
					Debug.mpl.info("10")
					self.handle_sync()
				elif data[0] == Globals.RESPONSE_TO_LOGIN_ATTEMPT:
					Debug.mpl.info("11")
					Debug.mpl.info("about to handle login response")
					self.handle_login_response(data)
					Debug.mpl.info("Finished to handle login response")
			except Exception as exc:
				try:
					Debug.mpl.error(exc)
					self.send_with_header(bytes([Globals.FAIL]) + bytes([Globals.MISC_ERROR]) + str.encode("Error: ") + str.encode(str(exc)))
				except ConnectionError:
					pass
				break
		Debug.mpl.info("exiting")
		self.users_data_mtx.acquire()
		if self.username in self.users_data:
			my_data = self.users_data[self.username]
			my_data["logged_in_machines"] -= 1
			self.users_data[self.username] = my_data
		self.users_data_mtx.release()
