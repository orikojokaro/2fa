#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "sqlite3.h"
#include "Utilities.h"
#include "exceptions.cpp"
#define ERRORS_LEN 100
#define COOKIE_LEN 16
#define FAIL 1
typedef unsigned char byte; 
class DBManager
{
	static sqlite3 * db;
	static std::vector<byte> cookie;
	static char* errors;
	static void resultHandler(int res);
	static void freeErrors();
public:
	//THROWS SqliteFailureExcpetion if failed to create table or if failed to open db!
	static void openDB();
	//THROWS SqliteFailureExcpetion if failed to create table or if failed to open db!
	static void openDB(std::string dbName);
	//THROWS CloseDBExcpeiton and SqliteFailureExcpetion!
	static void saveCookie( std::vector<byte> cookie);
	//THROWS CloseDBExcpeiton, NoCookieFoundException and SqliteFailureExcpetion!
	static std::vector<byte> getCookie();
	static bool isCookieStored();
	//THROWS SqliteFailureExcpetion and CloseDBException!
	static void delCookie();
	static void closeDB();
};

