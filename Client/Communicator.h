#pragma once
#include <winsock2.h>
#include <exception>
#include <ws2tcpip.h>
#include <string>
#include <string.h>
#include <vector>
#include<iostream>
#include "exceptions.cpp"
#define BUFF_LEN 1024
#define LEN_DESCRIPTION_LEN 2
#define DEFAULT_PORT "28050"
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
class Communicator
{
	static SOCKET connectSocket;
	static std::string ipAddr;
public:
	static void setCommunicationDetails(std::string ip);
	//THROWS CommunicationException!
	static void connectToServer();//used to connect to the server. must be used before send and recieve. 
	//THROWSCommunicationException and Unitialized conneciton!
	//toSend should not contain the full msg's length. it will be calculated in the fucntion.
	static void sendToServer( std::vector<byte> toSend) ;
	//THROWS CommunicationException and Unitialized conneciton!
	//will not return the  bytes describing the length of the full message.
	static std::vector<byte> receiveFromServer();
	//call closeCommunication to close the connection. Always call it if you want to exit peacefully.
	static void closeCommunication();

};

