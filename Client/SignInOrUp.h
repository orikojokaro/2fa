#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Communicator.h"
#include "DBManager.h"
#include "exceptions.cpp"
#include "Utilities.h"

#define SUCCESS 0
#define FAIL 1
#define FURTHER_AUTHENTICATION 2
#define LOGIN_DETAILS_AUTH 1
#define COOKIE_AUTH 2
#define SIGN_UP 0
#define CODE_LEN 1
#define MAX_LEN 255
#define MIN_PASS_LEN 8
#define MIN_USER_LEN 4
enum {
	DEFAULT,
	INCORRECT_DETAILS,
	TIME_OUT
};
struct LoginDetails {
	std::string username;
	std::string password;
};
//use askForLoginDetails to obtain the details from the user, then use loginDetailsAuth to perform the login.
//ALTERNATIVELY, use cookieAuth to perfrorm login using a cookie stored in the db.
class SignInOrUp
{
	LoginDetails* details = nullptr;
	LoginDetails* askUserForLoginDetails();
	std::vector<byte> handleUserPassContainingMsg(byte code);
public: 
	~SignInOrUp();
	void ResetLoginDetails();
	//message structure: |-1byte code|-1byte username len|username|passsword|
	//response's structure: |-1 byte fail or sucess|-byte error code, if failed. otherwise not existant|error message if failed or cookie if succeeded|
	//sends the logindetails to the servers to log in and returns the cookie
	//THROWS CommunicationExcpetion and UnitiatedConnection if there are sending errors or recieving errors!
	//THROWS MiscExcpetion!
	std::vector<byte> signUp();
	//message structure: |-1byte code|-1byte username len|username|passsword|
	//response's structure: |-1 byte fail or sucess|-byte error code, if failed. otherwise not existant|error message if failed or cookie if succeeded|
	//sends the logindetails to the servers to log in and returns the cookie
	//THROWS AuthenicationFailue if authenication failed
	//THROWS MiscExcpetion and ExceedingPendingRequestsLimit as well as LoginDenied!
	//THROWS CommunicationExcpetion and UnitiatedConnection if there are sending errors or recieving errors!
	std::vector<byte> loginDetailsAuth();

	//Authenticate using the cookie
	//message structure: |-1 byte code|cookie|
	//THROWS MiscExcpetion!
	//THROWS CommunicationExcpetion, UninitiatedCommunication and Authentication failue excpetion!
	void cookieAuth(std::vector<byte> cookie);
};
