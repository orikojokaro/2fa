#pragma once
#include <conio.h>
#include "Communicator.h"
#include "SignInOrUp.h"
#include "OthersLogin.h"
#include "DBManager.h"
#define HUNDREDTH_OF_A_SEC 10
#define NO_KEY_PRESSED 0
#define HUNDREDTH_OF_A_SEC_IN_SEC 100
/*Msg codes:
1 - login request
2 - cookie authentication
3 - sync login attempts
4 - get login attempt details
5 - response to login attempt
server answers:
true(1) -true/success
false(0) - false/failure
and my include additional information*/
class Manager
{
	//returns false if failed and the program shall terminate
	//THROWS CommunicationExcpetion
	bool manageSignUp();
	//THROWS CommunicationExcpetion
	bool manageLogin();//this handles loggin in from this device
	//THROWS communicationException
	void manageOthersLogin();//this handles allowing another device to log in
	//return true if yes
	bool getUserResponse();
	SignInOrUp SignObj;
public:
	void manage();
};

