#include "DBManager.h"

sqlite3* DBManager::db = nullptr;
char* DBManager::errors = nullptr;
std::vector<byte> DBManager::cookie;

void DBManager::resultHandler(int res)
{
	if (res != SQLITE_OK){
		char fixedSizeErrors[ERRORS_LEN] = { 0 };//prevent a memory leak.
		strcpy_s(fixedSizeErrors, ERRORS_LEN - 1, errors);//leaving 1 for the terminating null
		freeErrors();
		throw (SqliteFailureExcpetion(fixedSizeErrors));
	}
}

inline void DBManager::freeErrors()
{
	free(errors);
	errors = nullptr;
}

int callBackFindIfEmpty(void* data, int argc, char** argv, char** azColName)
{
	//if the callback was called, it means tables were found, so data which is a ptr to isEmpty is false;
	*(bool*)data = false;
	return 0;
}

void DBManager::openDB()
{
	int res = sqlite3_open("db", &db);
	bool isEmpty = false;
	if (res != SQLITE_OK) {
		db = nullptr;
		throw (SqliteFailureExcpetion("Failed to open DB"));
	}
	try {
		res = sqlite3_exec(db, "CREATE TABLE if not exists cookies("
			"cookie BLOB NOT NULL);", nullptr, nullptr, &errors);
		resultHandler(res);
	}
	catch (SqliteFailureExcpetion exc){
		if (db != nullptr) {
			sqlite3_close(db);
			db = nullptr;
		}
		throw exc;
	}
}


void DBManager::openDB(std::string dbName)
{
	int res = sqlite3_open(dbName.c_str(), &db);
	bool isEmpty = false;
	if (res != SQLITE_OK) {
		db = nullptr;
		throw (SqliteFailureExcpetion("Failed to open DB"));
	}
	try {
		isEmpty = true;//if a table does exist, the callback will change it to false.
		res = sqlite3_exec(db, "SELECT count(*) FROM sqlite_master WHERE type='table'", callBackFindIfEmpty, &isEmpty, &errors);
		resultHandler(res);
		if (isEmpty) {
			res = sqlite3_exec(db, "CREATE TABLE cookies("
				"cookie BLOB NOT NULL);", nullptr, nullptr, &errors);
			resultHandler(res);
		}
	}
	catch (SqliteFailureExcpetion exc) {
		if (db != nullptr) {
			sqlite3_close(db);
			db = nullptr;
		}
		throw exc;
	}
}

void DBManager::saveCookie(std::vector<byte> cookieToSet)
{
	if (nullptr == db) {
		throw(CloseDBException("You must open the db before accessing data!"));
	}
	DBManager::delCookie();
	//insert the new cookie
	int res = sqlite3_exec(db, (std::string("INSERT INTO cookies VALUES('") +
		Utilities::byteArrToStr(cookieToSet.data(), COOKIE_LEN) + "' );").c_str(),
		nullptr, nullptr, &errors);
	resultHandler(res);
	DBManager::cookie = cookieToSet;
}


int callBackGetCookie(void* data, int argc, char** argv, char** azColName)//copies the data to the given array
{
	using namespace std;
	for (int i = 0; i < COOKIE_LEN; i++)
	{
		((std::vector<byte>*)data)->push_back(argv[0][i]);
	}
	return 0;
}

 std::vector<byte> DBManager::getCookie()
{
	 if (nullptr == db) {
		 throw(CloseDBException("You must open the db before accessing data!"));
	 }
	if (0 == cookie.size()){
		//the callback will set the cookie
		int res = sqlite3_exec(db, "SELECT * FROM cookies", callBackGetCookie, &cookie, &errors);
		resultHandler(res);//here, I know that the db opened correctly and contains the table. 
		//is it possible to get an error?
		if (0 == cookie.size())
		{
			throw NoCookieFoundException();
		}
	}
	return cookie;
}

 //sets data to true, signing it was called.
 int callBackisCookieSet(void* data, int argc, char** argv, char** azColName)
 {
	 *(bool*)data = true;
	 return 0;
 }

 bool DBManager::isCookieStored()
 {
	 bool isStored = false;
	 if (nullptr == db) {
		 throw(CloseDBException());
	 }
	 if (0 == cookie.size()) {
		 //the callback will set the cookie
		 sqlite3_exec(db, "SELECT * FROM cookies", callBackisCookieSet, &isStored, nullptr);
	 }
	 else {
		 isStored = true;
	 }
	 return isStored;
 }

 void DBManager::delCookie()
 {
	 if (nullptr == db) {
		 throw(CloseDBException());
	 }
	 int res = sqlite3_exec(db, std::string("DELETE FROM cookies").c_str(),
		 nullptr, nullptr, &errors);
	 resultHandler(res);
	 cookie.resize(0);
 }

void DBManager::closeDB()
{
	sqlite3_close(db);
	db = nullptr;
}
