#include "Utilities.h"
bool Utilities::doesFileExist(const char* fileName)
{
	std::ifstream infile(fileName);
	return infile.good();
}
#include <iostream>
std::string Utilities::byteArrToStr(const byte byteArr[], int length)
{
	std::string str;
	str.resize(length);
	for (int i = 0; i < length;i++) {
		str[i] = (char)byteArr[i];//TODO ensure this conversion is legal
	}
	return str;
}

void Utilities::zeroStr(char str[], int length)
{
	for (int i = 0; i < length; i++)
	{
		str[i] = 0;
	}
}

byte Utilities::charToByte(char numChar)
{
	return numChar - ASCII_0;
}

 std::string Utilities::createErrorMsgFromByteVec(std::vector<byte> errorVec)
{
	std::string errorStr(errorVec.size() + 1, 0);
	errorStr.insert(errorStr.begin(), errorVec.begin(), errorVec.end());
	return errorStr;
}
