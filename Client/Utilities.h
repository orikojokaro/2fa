#pragma once
#include <fstream>
#include <string>
#include <vector>
#define ASCII_0 48
typedef unsigned char byte;
class Utilities
{
public:
	static bool doesFileExist(const char* fileName);
	static std::string byteArrToStr(const byte byteArr[], int length);
	static void zeroStr( char str[], int length);
	static byte charToByte( char numChar);
	static  std::string createErrorMsgFromByteVec(std::vector<byte> error);
};



