#pragma once
#include <iostream>
#include <vector>
#include "Communicator.h"
#include "exceptions.cpp"
#include "Utilities.h"

#define SYNC_CODE 3
#define LOGIN_ATTEMPT_DETAILS 4
#define LOGIN_ATTEMPT_RESPONSE 5
#define FAIL 1
#define SUCCESS 0
#define TRUE 1
#define CODE_LEN 1
#define LOC_OF_CONFORMATION_INDICATION 1
struct AttemptDetails
{
	std::string pcName;
	std::string location;
};
class OthersLogin
{
	bool isConfirmed();
public:
	//THROWS CommunicationException, UninitiatedConnectionExceptoin and MiscException
	bool isConformationRequired();
	//messgae structure: |-byte success code|-16bytes cookie |
	//response's strcuture  |-byte success code|if failed, byte error type |error message|
	//THROWS CommunicationException and Uninitiated connection Exceptoin
	//THROWS AuthenticationFailure
	//THROWS MiscExcpetion!
	void loginOther(std::vector<byte> cookie);
};

