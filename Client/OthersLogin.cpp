#include "otherslogin.h"

bool OthersLogin::isConformationRequired()
{
	std::vector<byte> toSend;
	std::vector<byte> response;
	toSend.push_back((byte)SYNC_CODE);
	Communicator::sendToServer(toSend);
	response = Communicator::receiveFromServer();
	if (response[0] == SUCCESS) {
		if (TRUE == response[LOC_OF_CONFORMATION_INDICATION]) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		throw MiscException((char *)std::vector<byte>(response.begin() + LOC_OF_CONFORMATION_INDICATION + sizeof(byte), response.end()).data());
	}
}

bool OthersLogin::isConfirmed()
{
	using namespace std;
	char response = 0;
	cout << "do you want to allow a PC to sign in to your account?\n"
		"y for yes, n for no\n";
	while (true)
	{
		cin >> response;//TODO ensure this won't break when entering a string;
		if ('y' == response)
		{
			return true;
		}
		else if ('n' == response)
		{
			return false;
		}
		cout << "invalid input. try again.\n";
	}
}

void OthersLogin::loginOther(std::vector<byte> cookie)
{
	std::vector<byte> toSend;
	std::vector<byte> recieve;
	toSend.push_back((byte)LOGIN_ATTEMPT_RESPONSE);
	if (isConfirmed()) {
		toSend.push_back((byte)true);
	}
	else {
		toSend.push_back((byte)false);
	}
	toSend.insert(toSend.end(), cookie.begin(), cookie.end());
	Communicator::sendToServer(toSend);
	recieve = Communicator::receiveFromServer();
	if (recieve[0] != TRUE) {
		if (AUTH_FAILURE == recieve[ERROR_SPECIFIER_LOC]) {
			throw AuthenticationFailure(Utilities::createErrorMsgFromByteVec(std::vector<byte>(recieve.begin() + CODE_LEN*2, recieve.end())));
		}
		else {
			throw MiscException(Utilities::createErrorMsgFromByteVec(std::vector<byte>(recieve.begin() + CODE_LEN * 2, recieve.end())));
		}
	}
}


