#include "Manager.h"

bool Manager::manageSignUp()
{
	using namespace std;
	while (true)
	{
		try
		{
			DBManager::saveCookie(SignObj.signUp());
			break;
		}
		catch (UsernameExists exc) {
			std::cout << "Failed to sign up because the username already exists.\nError: " <<
				exc.getError() << "\n"<<
				"Re enter your sign up details:\n";
			SignObj.ResetLoginDetails();
			continue;
		}
		catch (SqliteFailureExcpetion exc) {
			std::cout << "Failed to save the cookie due to DB failue.\nError: " <<
				exc.getError() << "\n" <<
				"The program will terminate.\n";
			system("PAUSE");
			return false;
		}
	}
	return true;
}

bool Manager::manageLogin()
{
	using namespace std;
	while (true)
	{
		if (DBManager::isCookieStored())
		{
			try
			{
				SignObj.cookieAuth(DBManager::getCookie());
				break;
			}
			catch (AuthenticationFailure exc) {
				std::cout << "Failed to log in due to an authenticaiton failue. Error:\n" <<
					exc.getError() << "\n";
				try {
					DBManager::delCookie();
				}
				catch (SqliteFailureExcpetion exc) {
					std::cout << "Failed to delete the cookie. Error:\n" <<
						exc.getError() << "\n"<<
						"It is advised to delete the database, named 'db', after the program will terminate.\n";
					system("PAUSE");
					return false;
				}
				continue;
			}
			catch (SqliteFailureExcpetion exc) {
				std::cout << "Failed to get the cookie due to DB failure. Error:\n" <<
					exc.getError() << "\n" <<
					"Would you like to try again?";
				if (getUserResponse()){
					continue;
				}
				else {
					return false;
				}
			}
		}
		else {
			try
			{
				DBManager::saveCookie(SignObj.loginDetailsAuth());
				break;
			}
			catch (ExceedingPendingRequestsLimit exc) {
				std::cout << "Only one login can be attempted at a time. Continue after confirmed the existing attempt. \nError message:\n" <<
					exc.getError() << "\n";
				system("Pause");
				continue;
			}
			catch (AuthenticationFailure exc) {
				std::cout << "Failed to log in due to an authenticaiton failue.\nError: " <<
					exc.getError() <<"\n";
				SignObj.ResetLoginDetails();
				continue;
			}
			catch (LoginDenied exc)
			{
				std::cout << "You weren't given permission to login from the other device. \nError:" <<
					exc.getError() << "\n";
				system("PAUSE");
				return false;
			}
			catch (SqliteFailureExcpetion exc) {
				std::cout << "Failed to save the cookie due to DB failue.\n Error: " <<
					exc.getError() << "\n"<<
				"The program will terminate.\n";
				system("PAUSE");
				return false;
			}
		}
	}
	return true;
}

void Manager::manageOthersLogin()
{
	using namespace std;
	int counter = 0;
	OthersLogin otherLogin;
	cout << "Waiting for login attempts.\n";
	while (true) {
		if (otherLogin.isConformationRequired()) {
			try
			{
				if (DBManager::isCookieStored()) {//cookie storing errors can occour
						otherLogin.loginOther(DBManager::getCookie());
						cout << "Sent confirmation to the server successfully\n";
				}
				else{
					std::cout << "Failed to extract the cookie. Close the program and then login again.\n";
					system("PAUSE");
					break;
				}
			}
			catch (AuthenticationFailure exc)
			{
				cout << "Failed to authenticate the login. \n"
					<<"Try to login again from the other PC to reinitiate the 2 factor authentication.\n"
					<<"Error msg: "
					<< exc.getError();
				system("PAUSE");
			}
			catch (SqliteFailureExcpetion exc) {
				std::cout << "Failed to extract the cookie. You might attempt to close the program and then login again.\n" <<
					"Error msg: \n" << exc.getError() << "\n";
				system("PAUSE");
				break;
			}
		}
		while (counter < HUNDREDTH_OF_A_SEC_IN_SEC)
		{
			if (NO_KEY_PRESSED != _kbhit()) {
				if (_getch() == '~')
				{
					break;
				}
			}
			Sleep(HUNDREDTH_OF_A_SEC);
			counter++;
		}
		counter = 0;
	}
}

bool Manager::getUserResponse()
{
	char response;
	std::cout << "Y for yes, anything for no.\n";
	std::cin >> response;
	if (response == 'y') {
		return true;
	}
	else{
		return false;
	}
}

void Manager::manage()
{
	using namespace std;
	char response;
	try
	{
		Communicator::connectToServer();
		DBManager::openDB();
	}
	catch (CommunicationException exc) {
		cout << "Failed to establish a connection with the server. Error message:\n" <<
			exc.getError() << "\n";
		system("PAUSE");
		return;
	}
	catch (SqliteFailureExcpetion exc) {
		cout << "Failed to Open/create a data base. Error message:\n" <<
			exc.getError() << "\n";
		system("PAUSE");
		Communicator::closeCommunication();
		return;
	}
	try
	{
		cout << "would you like to create a new account?";
		response = getUserResponse();
		if (response && manageSignUp()) {
				manageOthersLogin();
		}
		else if (manageLogin()) {
			manageOthersLogin();
		}
	}
	catch (CommunicationException exc)
	{
		cout << "A communication error has occured.\n" <<
			exc.getError() << "\n";
		system("PAUSE");
	}
	catch (MiscException exc)
	{
		cout << "An error has occured in the server.\nError:" <<
			exc.getError() << "\n";
		system("PAUSE");
	}
	cout << "Exiting...";
	Communicator::closeCommunication();
	DBManager::closeDB();
}
