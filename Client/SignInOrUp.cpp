#include "SignInOrUp.h"

LoginDetails* SignInOrUp::askUserForLoginDetails()
{
	using namespace std;
	LoginDetails* details = new LoginDetails;
	cout << "Username: ";
	cin >> details->username;
	while (details->username.length() < MIN_USER_LEN || details->username.length() > MAX_LEN)
	{
		std::cout << "the username's length must be above " << MIN_USER_LEN << " and below " << MAX_LEN << ". Try again.\nUsername: ";
		cin >> details->username;
	}
	cout << "Password: ";
	cin >> details->password;
	while (details->password.length() < MIN_PASS_LEN|| details->password.length() > MAX_LEN)
	{
		std::cout << "the password's length must be above "<< MIN_PASS_LEN<<" and below " <<MAX_LEN<<". Try again.";
		cin >> details->password;
	}
	return details;
}

std::vector<byte> SignInOrUp::handleUserPassContainingMsg(byte code)
{
	std::vector<byte> response;
	std::vector<byte> msg;
	msg.push_back(code);
	msg.push_back((byte)details->username.length());
	msg.insert(msg.end(), details->username.begin(), details->username.end());
	msg.insert(msg.end(), details->password.begin(), details->password.end());
	Communicator::sendToServer(msg);
	return Communicator::receiveFromServer();
}

SignInOrUp::~SignInOrUp()
{
	ResetLoginDetails();
}

void SignInOrUp::ResetLoginDetails()
{
	 if (nullptr != details) { 
		 delete details; 
	 }
	 details = nullptr;
}

std::vector<byte> SignInOrUp::signUp()
{
	if (nullptr == details) {
		details = askUserForLoginDetails();
	}
	std::vector<byte> response =handleUserPassContainingMsg(SIGN_UP);
	std::vector<byte> responseNoSucccessCode = std::vector<byte>(response.begin() + 1, response.end());
	if (response[0] == FAIL) {
		if (FAIL == response[0])
		{
			if (USERNAME_EXISTS == response[ERROR_SPECIFIER_LOC]) {
				throw UsernameExists(Utilities::createErrorMsgFromByteVec(responseNoSucccessCode));
			}
			else {
				throw MiscException(Utilities::createErrorMsgFromByteVec(std::vector<byte>(response.begin() + 1, response.end())));
			}
		}
	}
	else
	{
		return responseNoSucccessCode;
	}
}

std::vector<byte> SignInOrUp::loginDetailsAuth()
{
	if (nullptr == details) {
		details = askUserForLoginDetails();
	}
	std::vector<byte> response = handleUserPassContainingMsg(LOGIN_DETAILS_AUTH);
	std::vector<byte> responseNoSucccessCode = std::vector<byte>(response.begin() + CODE_LEN, response.end());
	std::vector<byte> responseNoFailureSpecifier;
	if (FAIL == response[0]){
		responseNoFailureSpecifier = std::vector<byte>(responseNoSucccessCode.begin() + CODE_LEN, responseNoSucccessCode.end());
		if (AUTH_FAILURE == response[ERROR_SPECIFIER_LOC] ){
			throw AuthenticationFailure(Utilities::createErrorMsgFromByteVec(responseNoFailureSpecifier));
		}
		else if (EXCEEDING_PENDING_REQUESTS_LIMIT == response[ERROR_SPECIFIER_LOC]) {
			throw ExceedingPendingRequestsLimit(Utilities::createErrorMsgFromByteVec(responseNoFailureSpecifier));
		}
		else {
			throw MiscException(Utilities::createErrorMsgFromByteVec(responseNoFailureSpecifier));
		}
	}
	else if (FURTHER_AUTHENTICATION == response[0])
	{
		std::cout << "Authorize the login on a different device you have. \n";
		std::cout << "waiting...\n";
		std::vector<byte> response = Communicator::receiveFromServer();
		responseNoSucccessCode = std::vector<byte>(response.begin() + 1, response.end());
		if (FAIL == response[0])
		{
			responseNoFailureSpecifier = std::vector<byte>(responseNoSucccessCode.begin() + CODE_LEN, responseNoSucccessCode.end());
			if (AUTH_FAILURE == response[ERROR_SPECIFIER_LOC]) {
				throw AuthenticationFailure(Utilities::createErrorMsgFromByteVec(std::vector<byte>(responseNoSucccessCode.begin() + CODE_LEN, responseNoSucccessCode.end())));
			}
			else if (LOGIN_DENIED == response[ERROR_SPECIFIER_LOC]) {
				throw LoginDenied(Utilities::createErrorMsgFromByteVec(std::vector<byte>(responseNoSucccessCode.begin() + CODE_LEN , responseNoSucccessCode.end())));
			}
			else {
				throw MiscException(Utilities::createErrorMsgFromByteVec(responseNoFailureSpecifier));
			}
		}
		return responseNoSucccessCode;
	}
	else
	{
		return responseNoSucccessCode;
	}
}

void SignInOrUp::cookieAuth(std::vector<byte> cookie)
{
	std::vector<byte> response;
	std::vector<byte> msg;
	msg.push_back((byte)COOKIE_AUTH);
	msg.insert(msg.begin() + 1, cookie.begin(), cookie.end());
	Communicator::sendToServer(msg);
	response = Communicator::receiveFromServer();
	if (FAIL == response[0]) {
		if (AUTH_FAILURE == response[ERROR_SPECIFIER_LOC]) {
			throw AuthenticationFailure(Utilities::createErrorMsgFromByteVec(std::vector<byte>(response.begin() + CODE_LEN + ERROR_SPECIFIER_LOC, response.end())));
		}
		else {
			throw MiscException(Utilities::createErrorMsgFromByteVec(std::vector<byte>(response.begin() + CODE_LEN  + ERROR_SPECIFIER_LOC, response.end())));
		}
	}
}
