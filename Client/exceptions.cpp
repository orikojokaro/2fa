#pragma once
#include <iostream>

#define OTHER_ERROR 255
#define USERNAME_EXISTS 0
#define UNINITIATED_CONNECTION 1
#define COMMUNICATION_EXCEPTION 2
#define CLOSE_DB 3
#define NO_COOKIE_FOUND 4
#define SQLITE_FAILURE 5
#define AUTH_FAILURE 6
#define EXCEEDING_PENDING_REQUESTS_LIMIT 7
#define LOGIN_DENIED 8
#define ERROR_SPECIFIER_LOC 1
#define MESSEGE_STARTS_WITH 2

class LoginDenied :public std::exception {
	std::string errorMsg = "";
public:
	LoginDenied(std::string error)
	{
		errorMsg = error;
	};
	std::string getError() { return errorMsg; };
};



class MiscException :public std::exception {
	std::string errorMsg = "";
public:
	MiscException(std::string error)
	{
		errorMsg = error;
	};
	std::string getError() { return errorMsg; };
};


class ExceedingPendingRequestsLimit :public std::exception {
	std::string errorMsg = "";
public:
	ExceedingPendingRequestsLimit(std::string error)
	{
		errorMsg = error;
	};
	std::string getError() { return errorMsg; };
};


class UsernameExists :public std::exception {
	std::string errorMsg = "";
public:
	UsernameExists(std::string error)
	{
		errorMsg = error;
	};
	std::string getError() { return errorMsg; };
};

class UninitiatedConnection :public std::exception {
	std::string errorMsg = "";
public:
	UninitiatedConnection(std::string error)
	{
		errorMsg = error;
	};
	std::string getError() { return errorMsg; };
};

class CommunicationException :public std::exception {
	std::string errorMsg = "";
	int code = 0;
public:
	CommunicationException(std::string error)
	{
		errorMsg = error;
	};
	CommunicationException(int errorCode)
	{
		code = errorCode;
	};
	CommunicationException(std::string error, int errorCode)
	{
		errorMsg = error;
		code = errorCode;
	};
	std::string getError() { return errorMsg; };
	int getErrorCode() { return code; };
};

class CloseDBException :public std::exception {
	std::string errorMsg = "";
public:
	CloseDBException()
	{
		errorMsg = "You must open the db before accessing data!";
	};
	CloseDBException(std::string error)
	{
		errorMsg = error;
	};

	std::string getError() { return errorMsg; };
};

class NoCookieFoundException :public std::exception {
	std::string errorMsg = "";
public:
	NoCookieFoundException()
	{
		errorMsg = "No cookie was found in the DB";
	};
	NoCookieFoundException(std::string error)
	{
		errorMsg = error;
	};

	std::string getError() { return errorMsg; };
};

class SqliteFailureExcpetion :public std::exception {
	std::string errorMsg = "";
public:
	SqliteFailureExcpetion(std::string error)
	{
		errorMsg = error;
	};
	std::string getError() { return errorMsg; };
};
class AuthenticationFailure :public std::exception
{
	std::string error;
public:
	AuthenticationFailure(std::string errorMsg)
	{
		error = errorMsg;
	}
	std::string getError() { return error; };
};