#include "Communicator.h"
SOCKET Communicator::connectSocket = INVALID_SOCKET;
std::string Communicator::ipAddr = "127.0.0.1";

void Communicator::setCommunicationDetails(std::string ip)
{
	ipAddr = ip;
}

void Communicator::closeCommunication()
{
	closesocket(connectSocket);
	WSACleanup();
}

void Communicator::connectToServer() 
{
	WSADATA wsaData;
	struct addrinfo* result = NULL,
		* ptr = NULL,
		hints;
	const char* sendbuf = "this is a test";
	int iResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		throw(CommunicationException("Winsock failed to initialize",iResult));
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(ipAddr.c_str(), DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		WSACleanup();
		throw(CommunicationException("getaddrinfo failed ", iResult));
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		connectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (connectSocket == INVALID_SOCKET) {
			WSACleanup();
			throw(CommunicationException("getaddrinfo failed ", WSAGetLastError()));
		}

		// Connect to server.
		iResult = connect(connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(connectSocket);
			connectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (connectSocket == INVALID_SOCKET) {
		WSACleanup();
		throw(CommunicationException("Unable to connect to server", iResult));
	}
}

void Communicator::sendToServer(std::vector<byte> toSend)
{
	if (INVALID_SOCKET == connectSocket)
	{
		throw (UninitiatedConnection("You must connect to the server first!"));
	}
	const char* arrPtr;
	int iResult;
	short int sizeOfMsg = (short int)toSend.size() + sizeof(short int);
	toSend.insert(toSend.begin(), (byte*)&sizeOfMsg, (byte*)&sizeOfMsg + sizeof(short int));
	arrPtr = (const char *)(toSend).data();
	iResult = send(connectSocket, arrPtr, toSend.size(), 0);//TODO ensure this works
	if (iResult == SOCKET_ERROR) {
		closesocket(connectSocket);
		WSACleanup();
		throw(CommunicationException("send failed with error: %d\n", WSAGetLastError()));
	}
}

std::vector<byte> Communicator::receiveFromServer()
{
	if (INVALID_SOCKET == connectSocket)
	{
		throw (UninitiatedConnection("You must connect to the server first!"));
	}
	int bytesRead = 0;
	char buffer[BUFF_LEN] = { 0 };
	std::vector<byte> result;
	short int totalLength = 0;
	//finding length of message
	int bytesReadTotal = bytesRead = recv(connectSocket, buffer, LEN_DESCRIPTION_LEN, 0);
	totalLength = *(short int *)buffer;
	while(true)
	{
		bytesRead = recv(connectSocket, buffer, BUFF_LEN, 0);
		bytesReadTotal += bytesRead;
		if (bytesRead > 0) {
			if (bytesReadTotal < totalLength)
			{
				result.insert(result.end(), buffer, buffer + BUFF_LEN);
			}
			else
			{
				result.insert(result.end(), buffer, buffer + bytesRead);
				break;
			}
		}
		else {
			throw(CommunicationException("recv failed.", WSAGetLastError()));
		}
	} 
	return result;
}
